
public class PairOfDices extends RollTheDices {
	
		
		
	    
	    public PairOfDices(){
	    	roll();
	    }


		public void roll() {
			die1 = 1+(int)(Math.random()*6);
			die2 = 1+(int)(Math.random()*6);
			}
		
		public int getDie1(){
			return die1;
		}
		public int getDie2(){
			return die2;
		}
		public int getTotal(){
			return die1+die2;
		}

	}

