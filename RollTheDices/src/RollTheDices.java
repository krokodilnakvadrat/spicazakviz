


public class RollTheDices {
	
	public static int die1;
    public static int die2;
	
    public static void main(String [] args){
		
		PairOfDices kockice = new PairOfDices();
		int brojBacanja = 0;
		
		
		do{
			kockice.roll();
			System.out.println("Kockice su pale na " + kockice.getDie1() + " i " + kockice.getDie2());
			brojBacanja++;
		}
		while(kockice.getTotal() != 2   );
		
		
		System.out.println("\nTrebalo je " + brojBacanja + " bacanja da bi zbir kockica bio 2.");
	
}
}